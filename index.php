<?php 
  include("db/db.php");
  include('inc/head_top.php'); 
  include('inc/head_menu.php'); 

  $imageBaseUrl="http://magicadmin.minusbugs.com/assets/";
  // $imageBaseUrl="http://localhost:82/tourismbackend/assets/";

    //All Package Details
  $sql_special = "SELECT packages.*,destinations.*FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId  WHERE packages.isSpecialPackage=1  order by packages.PackageId desc";
  $result_special = $conn->query($sql_special);
  //All Package Details
  $sql = "SELECT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId   order by packages.PackageId desc";
  $result = $conn->query($sql);

  //All Hotel Deatils
  $sql_hotel="SELECT hotel.*,location.LocationName FROM hotel INNER JOIN location on hotel.CountryId=location.LocationId ORDER BY hotel.HotelId DESC";
  $result_hotel=$conn->query($sql_hotel);

  //Cruise Deatils

  $sql_cruise="SELECT cruise.*,location.LocationName FROM cruise INNER JOIN location on cruise.CountryId=location.LocationId ORDER BY CruiseId DESC";
  $result_cruise=$conn->query($sql_cruise);


?>

      <div class="clearfix"></div>
      
      <!--==========================
      Intro Section
      ============================-->
      <div id="carouselExampleIndicators" class="carousel slide custom-carousel" data-ride="carousel">
        
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/slider-1.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1 class="animated slideInLeft">HONEYMOON</h1>
              <h2 class="animated slideInLeft">PACKAGES</h2>
              <p class="animated slideInLeft">Cuptatem poriatur molorio consere ceribea rcipsum que ressi tem<br>
                eostiis quodit evelend ionsedi ciaerem endeligenis eum iliquat dolorro<br>
                et, volupta sint quiaeriae dit venias a posseque cum corro quis mint officit<br>
              platiosapis estendam</p>
              <a href="#" class="btn btn-slider animated fadeInUp">KNOW MORE</a> <a href="contact.html" class="btn btn-slider animated fadeInUp">CONTACT US</a>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/slider-2.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1 class="animated slideInLeft">HONEYMOON</h1>
              <h2 class="animated slideInLeft">PACKAGES</h2>
              <p class="animated slideInLeft">Cuptatem poriatur molorio consere ceribea rcipsum que ressi tem<br>
                eostiis quodit evelend ionsedi ciaerem endeligenis eum iliquat dolorro<br>
                et, volupta sint quiaeriae dit venias a posseque cum corro quis mint officit<br>
              platiosapis estendam</p>
              <a href="#" class="btn btn-slider animated fadeInUp">KNOW MORE</a> <a href="contact.html" class="btn btn-slider animated fadeInUp">CONTACT US</a>
            </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/slider-3.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1 class="animated slideInLeft">HONEYMOON</h1>
              <h2 class="animated slideInLeft">PACKAGES</h2>
              <p class="animated slideInLeft">Cuptatem poriatur molorio consere ceribea rcipsum que ressi tem<br>
                eostiis quodit evelend ionsedi ciaerem endeligenis eum iliquat dolorro<br>
                et, volupta sint quiaeriae dit venias a posseque cum corro quis mint officit<br>
              platiosapis estendam</p>
              <a href="#" class="btn btn-slider animated fadeInUp">KNOW MORE</a> <a href="contact.html" class="btn btn-slider animated fadeInUp">CONTACT US</a>
            </div>
            
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/slider-4.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block text-left">
              <h1 class="animated slideInLeft">HONEYMOON</h1>
              <h2 class="animated slideInLeft">PACKAGES</h2>
              <p class="animated slideInLeft">Cuptatem poriatur molorio consere ceribea rcipsum que ressi tem<br>
                eostiis quodit evelend ionsedi ciaerem endeligenis eum iliquat dolorro<br>
                et, volupta sint quiaeriae dit venias a posseque cum corro quis mint officit<br>
              platiosapis estendam</p>
              <a href="#" class="btn btn-slider animated fadeInUp">KNOW MORE</a> <a href="contact.html" class="btn btn-slider animated fadeInUp">CONTACT US</a>
            </div>
            
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!--==========================
      Special Offers
      ============================-->
      <section id="special-offers">
        <div class="setion-title text-center">
          <h2>SPECIAL OFFERS</h2>
          <h4>CLAIM THE BEST</h4>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">

              
              
              <div class="owl-carousel-one owl-carousel owl-theme">
                <?php
                while($row_special = $result_special->fetch_assoc()) {
                ?>
                
                
                <div class="item-container item"> 
                  <img src="<?php echo $imageBaseUrl; ?>PackageImages/<?php echo $row_special["PackageImage"] ?>" class="img-fluid image package-small-image" alt="special-offers"/>
                  <div class="image-ovrtxt-pckages">
                    <p><?php echo $row_special["PackageTitle"] ?></p>
                  </div>
                  <div class="blue-gradien-color"></div>
                  <div class="prferedhotels"><div class="pull-right"><a href="#" class="btn btn-tp" role="button">
                    <span class="package-amount"><i class="fa fa-inr"></i> <?php echo $row_special["PackageAmount"] ?></span>
                  </a></div><div class="prferedhotelstext"><?php echo $row_special["Days"] ?><br>

                  <span class="fa fa-star <?php echo $row_special["Rating"]>=1?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row_special["Rating"]>=2?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row_special["Rating"]>=3?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row_special["Rating"]>=4?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row_special["Rating"]>=5?'checked':'' ?>"></span>
                </div></div>
                <div class="overlay-offer overlay-tp">
                  <div class="overlay-text">
                    <p class="light-text"><?php echo $row_special["PackageTitle"] ?></p>
                    <p<?php echo $row_special["PackageLocation"] ?></p>
                    <div class="d-block offer-icons">
                      <span class="icon-hotel"></span> 
                      <span class="icon-fod"></span> 
                      <span class="icon-adventur"></span>
                      <span class="icon-flight"></span> 
                      <span class="icon-tour"></span> 
                    </div>
                      <div class="buttons"> 
                        <a href="packages-inner.php?id=<?php echo $row_special["PackageId"] ?>" class="btn btn-transparent"><i class="fa fa-link"></i> more</a>  
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clerafix"></div>
      <!--==========================
      CRUISE PACKAGES
      ============================-->
      <section id="cruise-packages">
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row">
                <div class="col-12 col-sm-4 col-lg-4">
                  <div class=" white-title-text">
                    <h2>CRUISE <br>
                    PACKAGES</h2>
                    <h4>EXPLORE THE SEA ROUTES</h4>
                  </div>
                  <div class="clearfix"></div>
                  <a href="#" class="btn btn-view">view all</a> </div>
                  <div class="col-12 col-sm-8 col-lg-8">
                    <div class="owl-carousel-two owl-carousel owl-theme">
                    <?php
                      while($row_crusie = $result_cruise->fetch_assoc()) {
                    ?>
                      <div class="item item-container"> 
                        <img src="<?php echo $imageBaseUrl; ?>CruiseImages/<?php echo $row_crusie["CruiseImage"] ?>" class="img-fluid image cruise-small-image" alt="special-offers">
                        <div class="cp-image-text">
                          <h4><?php echo $row_crusie["CruiseName"] ?></h4>
                          <div class="row no-gutters">
                            <div class="col-4 col-sm-5 col-lg-6">
                              <p><?php echo $row_crusie["CruiseDays"] ?></p>
                            </div>
                            <div class="col-4 col-sm-4 col-lg-3 mag-sli-p">
                              <p><i class="fa fa-anchor" ></i> <?php echo $row_crusie["LocationName"] ?></p>
                            </div>
                            <div class="col-4 col-sm-3 col-lg-3 mag-sli-p"><p><i class="fa fa-usd" ></i> <b><?php echo $row_crusie["CruiseRate"] ?></b></p></div>
                          </div>
                          <div class="ship-name"><i class="fa fa-ship"></i><?php echo $row_crusie["CruiseType"] ?></div>
                          <div class="location"><i class="fa fa-map-marker"></i><?php echo $row_crusie["CruiseLocation"] ?></div>
<!--                           <table class="table">
                            <tbody>
                              <tr>
                                <td><span class="icon-departures"></span></td>
                                <td>2018 :</td>
                                <td>Jan 26, Mar 22, Jan 26, Mar 22, <br>
                                Jan 26, Mar 22, Jan 26, Mar 22</td>
                              </tr>
                              <tr>
                                <td></td>
                                <td>2019 :</td>
                                <td>Jan 20, Mar 23</td>
                              </tr>
                            </tbody>
                          </table> -->
                        </div>
                        <div class="overlay-offer">
                          <div class="overlay-text"> <img src="img/c1.jpg" class="img-fluid ship-logo" alt="c1">
                            <p class="strong-text"><?php echo $row_crusie["CruiseName"] ?></p>
                            <p><?php echo $row_crusie["CruiseDays"] ?></p>
                            <div class="d-block offer-icons"> <span class="icon-hotel"></span> <span class="icon-fod"></span> <span class="icon-adventur"></span> <span class="icon-tour"></span> </div>
                            <div class="buttons"> <a href="#" class="btn  btn-transparent"><i class="fa fa-link"></i> more</a> </div>
                          </div>
                        </div>
                      </div>
                      <?php
                    }?>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="clearfix"></div>
        <!--==========================
        #  TOUR PACKAGES
        ============================-->
        <section id="tour-packages">
          <div class="setion-title text-center">
            <h2>TOUR PACKAGES</h2>
            <h4>TOUR THE WORLD</h4>
          </div>
          <div class="container-fluid">
            <div class="row">
              <div class="container">
                <div class="owl-carousel-three owl-carousel owl-theme">
                            <?php
               
                $result = $conn->query($sql);
                while($row = $result->fetch_assoc()) {
                
                ?>
                
                
                <div class="item-container item"> <img src="<?php echo $imageBaseUrl; ?>PackageImages/<?php echo $row["PackageImage"] ?>" class="img-fluid image package-small-image" alt="special-offers">
                  <div class="image-ovrtxt-pckages">
                    <p><?php echo $row["PackageTitle"] ?></p>
                  </div>
                  <div class="blue-gradien-color"></div>
                  <div class="prferedhotels"><div class="pull-right"><a href="#" class="btn btn-tp" role="button">
                    <span class="package-amount"><i class="fa fa-inr"></i> <?php echo $row["PackageAmount"] ?></span>
                  </a></div><div class="prferedhotelstext"><?php echo $row["Days"] ?><br>
                  <span class="fa fa-star <?php echo $row["Rating"]>=1?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row["Rating"]>=2?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row["Rating"]>=3?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row["Rating"]>=4?'checked':'' ?>"></span>
                  <span class="fa fa-star <?php echo $row["Rating"]>=5?'checked':'' ?>"></span>
                </div></div>
                <div class="overlay-offer overlay-tp">
                  <div class="overlay-text">
                    <p class="light-text"><?php echo $row["PackageTitle"]; ?></p>
                    <p<?php echo $row["PackageLocation"] ?></p>
                    <div class="d-block offer-icons">
                      <span class="icon-hotel"></span> 
                      <span class="icon-fod"></span> 
                      <span class="icon-adventur"></span>
                      <span class="icon-flight"></span> 
                      <span class="icon-tour"></span> 
                    </div>
                      <div class="buttons"> 
                        <a href="packages-inner.php?id=<?php echo $row["PackageId"] ?>" class="btn btn-transparent"><i class="fa fa-link"></i> more</a>  
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                }
                ?>
      
    </div>
  </div>
</div>
</div>
</section>
<div class="clearfix"></div>
<!--==========================
Prefered Hotels
============================-->
<section id="prefered-hotels">
<div class="overlay"></div>
<div class="container">
<div class="row">
  <div class="col-12 col-sm-4 col-lg-4">
    <div class=" white-title-text">
      <h2>PREFERED <br>
      HOTELS</h2>
      <h4>LUXURY AROUND WORLD</h4>
    </div>
    <div class="clearfix"></div>
    <a href="#" class="btn btn-view">view all</a> </div>
    <div class="col-12 col-sm-8 col-lg-8">
      <div class="owl-carousel-four owl-carousel owl-theme">
        <?php
        while($row_hotel = $result_hotel->fetch_assoc()) {
        ?>
        
        <div class="item item-container">
          <img src="<?php echo $imageBaseUrl; ?>HotelImages/<?php echo $row_hotel["HotelImage"] ?>" class="img-fluid image hotel-small-image" alt="special-offers">
          <div class="prferedhotels">
            <div class="row">
              <div class="col-md-6">
                <div class="prferedhotelstext"><?php echo $row_hotel["HotelName"] ?></div>
              </div>
              <div class="col-md-6">
                <a href="#" class="btn btn-sm" role="button">Per night<br> $ <?php echo $row_hotel["HotelRate"] ?></a>
              </div>
            </div>
            <div class="pull-right">
              
            </div>
            
          </div>
          <div class="overlay-prefferd-hotels">
            <div class="overlay-text">
              <p class="strong-text"><?php echo $row_hotel["HotelName"] ?></p>
              <p><?php echo $row_hotel["HotelLocation"] ?>,<?php echo $row_hotel["LocationName"] ?></p>
              <div class="d-block"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
              <p class="price">Per night <i class="fa fa-usd"></i> <?php echo $row_hotel["HotelRate"] ?></p>
              <div class="buttons"> 
                <a href="#" class="btn btn-transparent btn-white">
                  <i class="fa fa-link"></i> more
                </a>  
              </div>
            </div>
          </div>
        </div>
        <?php
        }
        ?>
        
        
      </div>
    </div>
  </div>
</div>
</section>

<!--==========================
# TODAY’S DESTINATIONS
============================-->

<section id="todays-destination">
<div class="container shadow-box">
  <div class="row">
    <div class="col-12 col-sm-6 col-lg-6 no-lp"> <img src="img/tag-mahal.jpg" class="img-fluid rounded-image"> </div>
    <div class="col-12 col-sm-6 col-lg-6">
      <div class="tds-dest-pad">
        <div class="setion-title">
          <h2>TODAY’S <br>
          DESTINATIONS</h2>
        </div>
        <div class="offset-md-1 col-md-11">
          <h4>India | Asia</h4>
          <p> Tourism in India has shown a phenomenal growth
            in the past decade. One of the reasons is that the
            Ministry of tourism, India has realized the immense
            potential of tourism in India during vacations. India
            travel tourism has grown rapidly with a great influx
          of tourists from all across the globe.. </p>
          <div class="buttons float-left"> <a href="#" class="btn btn-shade"><i class="fa fa-link"></i> more</a> <a href="#" class="btn btn-shade btn-n0-shade"><i class="fa fa-cart-plus"></i> to cart</a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<div class="clearfix"></div>
<!--==========================
Explore Qatar
============================-->

<section id="exploreqatar">
<div class="parallax-destinations">
  <div class="container">
    <div class="setion-title">
      <h2 class="text-center">EXPLORE QATAR</h2>
      <h4 class="text-center">PLACES SPEAK BEAUTY</h4>
    </div>
    <div class="row row-flex">
      <div class="col-sm-12 col-md-5 mt-4"><img src="img/qatar.jpg" class="img-fluid" alt="qatar">
        <div class="text-overlay">
          <div class="text"> explore<strong>qatar</strong>
            <p>The Pearl-Qatar | Doha Corniche | Al Wakrah</p>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-7">
        <div class="row">
          <div class="col-sm-12 col-md-6  mt-4"><img src="img/himalayam.jpg" class="img-fluid" alt="">
            <div class="right-text">Himalayan<strong>Treasures</strong>
              <p>Mount Everest | Nanga Parbat | Manaslu</p>
            </div>
          </div>
          <div class="col-sm-12 col-md-6  mt-4"><img src="img/ZurichCityBreak.jpg" class="img-fluid" alt="">
            <div class="right-text">Zurich<strong>City Break</strong>
              <p>Grossmünster | Fraumünster | Lake Zurich</p>
            </div>
          </div>
          <div class="col-sm-12 col-md-6  mt-4"><img src="img/DublinGetaway.jpg" class="img-fluid" alt="">
            <div class="right-text">Dublin<strong>Getaway</strong>
              <p>Mount Everest | Nanga Parbat | Manaslu</p>
            </div>
          </div>
          <div class="col-sm-12 col-md-6  mt-4"><img src="img/coorg.jpg" class="img-fluid" alt="">
            <div class="right-text">Cool<strong>Coorg</strong>
              <p>Grossmünster | Fraumünster | Lake Zurich</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!--==========================
Featured from Us
============================-->
<section id="featured">
<div class="overlay-featured"></div>
<div class="container-fluid">
  <div class="setion-title text-center">
    <h2>FEATURED FROM US</h2>
    <h4>WE GIVE YOU THE BEST</h4>
  </div>
  <div class="container content-area">
    <div class="row">
      <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/15featured.jpg" class="img-fluid" alt="13featured"> </div>
      <div class="col-12 col-sm-3 col-lg-3 no-gutters">
        <div class="featured-content">
          <h3>INDIA</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
          <a href="#" class="btn btn-1 btn-white">more</a> </div>
        </div>
        <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/14featured.jpg" class="img-fluid" alt="14featured"> </div>
        <div class="col-12 col-sm-3 col-lg-3 no-gutters">
          <div class="featured-content">
            <h3>honey moon</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            <a href="#" class="btn btn-1 btn-white">more</a> </div>
          </div>
          <div class="col-12 col-sm-3 col-lg-3 no-gutters">
            <div class="featured-content">
              <h3>honey moon</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
              <a href="#" class="btn btn-1 btn-white">more</a> </div>
            </div>
            <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/15featured.jpg" class="img-fluid" alt="15featured"> </div>
            <div class="col-12 col-sm-3 col-lg-3 no-gutters">
              <div class="featured-content">
                <h3>honey moon</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                <a href="#" class="btn btn-1 btn-white">more</a> </div>
              </div>
              <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/16featured.jpg" class="img-fluid" alt="16featured"> </div>
              <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/16featured.jpg" class="img-fluid" alt="16featured"> </div>
              <div class="col-12 col-sm-3 col-lg-3 no-gutters">
                <div class="featured-content">
                  <h3>honey moon</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                  <a href="#" class="btn btn-1 btn-white">more</a> </div>
                </div>
                <div class="col-12 col-sm-3 col-lg-3 no-gutters"> <img src="img/14featured.jpg" class="img-fluid" alt="16featured"> </div>
                <div class="col-12 col-sm-3 col-lg-3 no-gutters">
                  <div class="featured-content">
                    <h3>honey moon</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <a href="#" class="btn btn-1 btn-white">more</a> </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!--==========================
          TESTIMONIALS Section
          ============================-->
          <section id="testimonials">
            <div class="container">
              <div class="row">
                <div class="col-12 col-sm-6 col-lg-7 shadow-box">
                  
                  <div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
                    
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                      <li data-target="#testimonialCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#testimonialCarousel" data-slide-to="1"></li>
                      <li data-target="#testimonialCarousell" data-slide-to="2"></li>
                    </ul>
                    
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <div class="padd-cls">
                          <blockquote>
                            <div class="row">
                              <div class="col-12 col-sm-3 col-lg-3 text-center marg-testi">
                                <img class="img-fluid img-circle cust-img" src="img/17testimonials.jpg" >
                                <h3>RAHUL PR</h3>
                                <p>Doha, Qatar</p>
                              </div>
                              <div class="d-none d-lg-block d-sm-block col-sm-2 col-lg-2">
                                <img src="img/testimonial-element.png" class="img-fluid tstlm-elmnt" alt="testimonial-element">
                              </div>
                              <div class="col-12 col-sm-7 col-lg-7 mrgn-fr-cntnt">
                                <div class="setion-title text-center">
                                  <h2>TESTIMONIALS</h2>
                                </div>
                                <p class="text-justify">
                                  Arrival, meet greet and proceed to Kandalama via Pinnawala.
                                  The Pinnawela Elephant Orphanage, established in 1975,
                                  commenced with seven orphans. Today some of these
                                  orphans enjoy the fortune of seeing Elephant Orphanage,
                                  established in 1975, commenced with seven orphans. Today
                                  some of these orphans enjoy the fortune of seeing their
                                  grandchildren born in the same location....
                                </p>
                                <a href="#" class="pull-right">More +</a>
                                
                              </div>
                            </div>
                          </blockquote>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <div class="padd-cls">
                          <blockquote>
                            <div class="row">
                              <div class="col-12 col-sm-3 col-lg-3 text-center marg-testi">
                                <img class="img-fluid img-circle cust-img" src="img/17testimonials.jpg" >
                                <h3>RAHUL PR</h3>
                                <p>Doha, Qatar</p>
                              </div>
                              <div class="d-none d-lg-block d-sm-block col-sm-2 col-lg-2">
                                <img src="img/testimonial-element.png" class="img-fluid tstlm-elmnt" alt="testimonial-element">
                              </div>
                              <div class="col-12 col-sm-7 col-lg-7 mrgn-fr-cntnt">
                                <div class="setion-title text-center">
                                  <h2>TESTIMONIALS</h2>
                                </div>
                                <p class="text-justify">
                                  Arrival, meet greet and proceed to Kandalama via Pinnawala.
                                  The Pinnawela Elephant Orphanage, established in 1975,
                                  commenced with seven orphans. Today some of these
                                  orphans enjoy the fortune of seeing Elephant Orphanage,
                                  established in 1975, commenced with seven orphans. Today
                                  some of these orphans enjoy the fortune of seeing their
                                  grandchildren born in the same location....
                                  
                                </p>
                                <a href="#" class="pull-right">More +</a>
                              </div>
                            </div>
                          </blockquote>
                        </div>
                      </div>
                      <div class="carousel-item ">
                        <div class="padd-cls">
                          <blockquote>
                            <div class="row">
                              <div class="col-12 col-sm-3 col-lg-3 text-center marg-testi">
                                <img class="img-fluid img-circle cust-img" src="img/17testimonials.jpg" >
                                <h3>RAHUL PR</h3>
                                <p>Doha, Qatar</p>
                              </div>
                              <div class="d-none d-lg-block d-sm-block col-sm-2 col-lg-2">
                                <img src="img/testimonial-element.png" class="img-fluid tstlm-elmnt" alt="testimonial-element">
                              </div>
                              <div class="col-12 col-sm-7 col-lg-7 mrgn-fr-cntnt">
                                <div class="setion-title text-center">
                                  <h2>TESTIMONIALS</h2>
                                </div>
                                <p class="text-justify">
                                  Arrival, meet greet and proceed to Kandalama via Pinnawala.
                                  The Pinnawela Elephant Orphanage, established in 1975,
                                  commenced with seven orphans. Today some of these
                                  orphans enjoy the fortune of seeing Elephant Orphanage,
                                  established in 1975, commenced with seven orphans. Today
                                  some of these orphans enjoy the fortune of seeing their
                                  grandchildren born in the same location....
                                </p>
                                <a href="#" class="pull-right">More +</a>
                              </div>
                            </div>
                          </blockquote>
                        </div>
                      </div>
                    </div>
                    
                    <!-- Left and right controls -->
                    <div class="testi-arrows">
                      <a class="carousel-control-prev1 float-left" href="#testimonialCarousel" data-slide="prev"> <img src="img/arrow-left.png" class="img-fluid"> </a>
                      <a class="carousel-control-next1 " href="#testimonialCarousel" data-slide="next"> <img src="img/arrow-right.png" class="img-fluid"> </a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-5 subscribe-section">
                  <div class="setion-title">
                    <h2>SUBSCRIBE </h2>
                    <p>TO OUR NEWSLETTER</p>
                  </div>
                  <p> El int quae. Lorupta dolupicim estores explaccum quam,
                  auda del is quamus maio volor aperci </p>
                  <div class="email-subscription">
                    <input type="email" placeholder="Email Address" required class="form-control mr-2">
                    <button type="submit" class="btn mt-3  btn-shade btn-sub">Send now</button>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
          <!--==========================
          Footer Section
          ============================-->
          
          
          
          <footer>
            <div class="overlay"></div>
            <div class="container">
              <div class="row">
                <div class="col-12 text-center mt-2 mb-4"> <img src="img/logo footer.png" class="img-fluid" alt="logo_footer"> </div>
                <div class="col-12 col-sm-3 col-lg-3">
                  <div class="footer-contents">
                    <h3>Contact Us</h3>
                    <p>MAGIC TOURS <br>
                      PO Box 80915, Building 14, Shop 34<br>
                      Barwa Village, Al Wakra Road, <br>
                    Doha, State of Qatar </p>
                    <!--<div class="row">
                      <div class="col-12 col-sm-6">
                        <div class="gloat-left"><span class="icon-callus"></span>+974 449 17 303 </div>
                      </div>
                      <div class="col-12 col-sm-6">
                        <div class="float-left"><span class="icon-fax"></span>+974 449 17 308
                      </div>
                    </div>
                  </div>-->
                  <ul class="cont-info">
                    <li>+974 449 17 303</li>
                    <li>+974 449 17 308</li>
                  </ul>
                  <p class="email">info@magictoursonline.com</p>
                  <p class="web">www.magictoursonline.com</p>
                  
                </div>
              </div>
              <div class="col-12 col-sm-2 ftr-brdr-cls">
                <div class="footer-contents">
                  <h3>Booking</h3>
                  <ul class="footer-list">
                    <li>Best Deals</li>
                    <li>BuyersProtection</li>
                    <li>Roomers top 100</li>
                  </ul>
                </div>
              </div>
              <div class="col-12 col-sm-3 ftr-brdr-cls">
                <div class="footer-contents">
                  <h3>Support</h3>
                  <ul class="footer-list">
                    <li>Trust + Safety</li>
                    <li>FAQs</li>
                    <li>Testimonials</li>
                    <li>Contact Us</li>
                    <li>Hotel Cancellation Policy</li>
                    <li>Cards</li>
                    <li>my permissions</li>
                  </ul>
                </div>
              </div>
              <div class="col-12 col-sm-2 ftr-brdr-cls">
                <div class="footer-contents">
                  <h3>Company</h3>
                  <ul class="footer-list">
                    <li>About Us</li>
                    <li>Press</li>
                    <li>Blog</li>
                    <li>Privacy Policy</li>
                    <li>Terms & Conditions</li>
                    <li>Partner With Us</li>
                    <li>my permissions</li>
                  </ul>
                </div>
              </div>
              <div class="col-12 col-sm-2 ftr-brdr-cls">
                <div class="footer-contents">
                  <h3>Top Destinations</h3>
                  <div class="row">
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/02special.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/03special.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/destinations.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/03special.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/destinations.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col-4 col-md-4 ftr-company-pic">
                      <a href="#"><img src="img/02special.jpg" class="img-fluid"></a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ftr-socio-icons">
                <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
              </div>
              <div class="clearfix"></div>
              <div class="ftr-copyright text-center">
                <p>© 2018 Magic Tours allright reserved</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- JavaScript Libraries -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/main.js"></script>
      <!--fixed nav-->
      </script>
      <!-- menu -->
      <script>
      $(document).ready(function() {
      
     
      // executes when HTML-Document is loaded and DOM is ready
      // breakpoint and up
      $(window).resize(function(){
          if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
      $(this).parent().toggleClass("show");
      $(this).parent().find(".dropdown-menu").toggleClass("show");
      });
      // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
      $(this).removeClass("show");
      });
      
          // do something here
          }
      });
      
      
      // document ready
      });
      </script>
      <script src="js/swipper.js"></script>
      <!-- prefered hotels -->
      <script src="js/owl.carousel.min.js"></script>
      <!--<script>
        
          $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
      0:{
      items:1
      },
      600:{
      items:3
      },
      1000:{
      items:3
      }
      }
      })
      </script> -->
      <script>
      $(document).ready(function(){
        
        $('.owl-carousel-one').owlCarousel({
      loop:true,
      margin:40,
      nav:true,
      responsive:{
      0:{
      items:1
      },
      600:{
      items:3
      },
      1000:{
      items:4
      }
      }
      });
        $('.owl-carousel-two').owlCarousel({
      loop:true,
      margin:35,
      nav:true,
      responsive:{
      0:{
      items:1
      },
      600:{
      items:3
      },
      1000:{
      items:2
      }
      }
      });
        $('.owl-carousel-three').owlCarousel({
      loop:true,
      margin:35,
      nav:true,
      responsive:{
      0:{
      items:1
      },
      600:{
      items:3
      },
      1000:{
      items:4
      }
      }
      });
        $('.owl-carousel-four').owlCarousel({
      loop:true,
      margin:35,
      nav:true,
      responsive:{
      0:{
      items:1
      },
      600:{
      items:3
      },
      1000:{
      items:3
      }
      }
      });
      });
      </script>
      <script>
      window.onscroll = function() {myFunction()};
      var header = document.getElementById("myHeader");
      var sticky = header.offsetTop;
      function myFunction() {
      if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
      } else {
      header.classList.remove("sticky");
      }
      }
      </script>
      <script src="js/classie.js"></script>
      <script src="js/uisearch.js"></script>
      <script>
      new UISearch( document.getElementById( 'sb-search' ) );
      </script>
    </body>
  </html>