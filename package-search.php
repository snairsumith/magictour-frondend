<?php
include("db/db.php");
include('inc/head_top.php');
include('inc/head_menu.php');
$sql_type="SELECT * FROM tour_types";
$result_types = $conn->query($sql_type);
$sql_destination="SELECT * FROM location where TypeOfLocation=1";
$result_destination=$conn->query($sql_destination);
$result_destination_1=$conn->query($sql_destination);
?>
<div class="clearfix"></div>
<section class="about-header">
  <div class="container">
    <div class="row">
      <div class="offset-md-3 col-md-6 offset-right-md-3 text-center">
        <div class="about-banner-title text-center"><p><a href="index.php">Home</a> > <a href="#">Packages Search</a></p>
        
        <h1>Packages</h1>
      </div>
    </div>
    
    
  </div>
</div>
</section>
<section id="packages">
<div class="container">
  <div class="row">
    <div class="d-none d-sm-block col-sm-3" id="sticky-sidebar">
      <div class="sticky-top">
        <div class="promotion-slider">
          <h3>EXCLUSIVE OFFERS</h3>
          <div id="promotionSlider" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#promotionSlider" data-slide-to="0" class="active"></li>
              <li data-target="#promotionSlider" data-slide-to="1"></li>
              <li data-target="#promotionSlider" data-slide-to="2"></li>
            </ul>
            
            <!-- The slideshow -->
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
              </div>
              <div class="carousel-item">
                <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
              </div>
              <div class="carousel-item">
                <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
              </div>
              <div class="carousel-item">
                <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
              </div>
              
            </div>
          </div>
          
        </div>
        
        <div class="no-of-days mt-5">
          <h3>NO. OF DAYS</h3>
          <div ng-app="myapp">
            <div ng-controller="TestController as vm">
               
              <rzslider rz-slider-model="vm.priceSlider.value" rz-slider-options="vm.priceSlider.options"></rzslider>
            </div>
          </div>
          
          
        </div>
        
        <div class="sidebar-links mt-5">
          <h3>TYPES</h3>
          <div class="padding-sidebar">
            <ul>
              <?php
              while($row_types = $result_types->fetch_assoc()) {
              ?>
              
              <li> 
                <input type="checkbox" name="rdTourType" onclick="checkboxTourClick(<?php echo $row_types['TypeId'] ?>,'<?php echo $row_types['TypeName'] ?>')" id="chkBox_<?php echo $row_types['TypeId'] ?>" value="<?php echo $row_types['TypeId'] ?>" class="float-right"> <?php echo $row_types['TypeName'] ?>
              </li>
              <?php
              }
              ?>
              <input type="hidden" name="hdType" id="hdType" value=[]>
              <input type="hidden" name="hdType" id="hdDestination" value=0>
            </ul>
          </div>
        </div>
        </div> <!--end sticky-->
        
      </div>
      
      <div class="col-12 col-sm-9" id="main">
        <div class="container">
          <div class="row">
            <div class="package-content">
              <h3>HOLIDAYS PACKAGES</h3>
              <p class="text-justify">East or West, Honey Moon or Family Holiday, Adventure Trips or Back pack programs, Magic Tours has it all. Please select
                your preferred destinations and we are more than glad to customize it at your wish. Our service starts from pick you up
                from you home until drop you back at home after the great holiday. We are pleased to handle your flights, hotels, local
              travel and tour arrangements etc. at all budgets and comforts. Please share your dream and we design your holidays.</p>
            </div>
            <div class="pack-main-cont-cls">
              <div class="pack-check-head">
                <h5>Destinations</h5>
              </div>
              <div class="checkbox" id="filter">
                <?php
                while($row_destination = $result_destination->fetch_assoc()) {
                ?>
                <label>
                  <input type="checkbox" class="kraj main-cat" id="<?php echo $row_destination['LocationId'] ?>" value="<?php echo $row_destination['LocationId'] ?>" onClick="checkThis(this);"><?php echo $row_destination['LocationName'] ?>


                

                </label>
                <?php
                }
                ?>
              </div>
              <?php
              while($row_destination_1 = $result_destination_1->fetch_assoc()) {
              ?>
              <div class="check-box-pack-inn" id="<?php echo $row_destination_1['LocationId'] ?>-div">
                <h5><?php echo $row_destination_1['LocationName'] ?></h5>
                <div class="row">
                <?php 
                  $paraentId =$row_destination_1['LocationId'];
                  $sql_destination_child="select * from location where ParentId=$paraentId";
                  $result_destination_child=$conn->query($sql_destination_child);
                  while($row_destination_child = $result_destination_child->fetch_assoc()) {
                ?>
                <div class="col-md-3">
                          <label>
                            <input 
                            type="checkbox" 
                            class="kraj" 
                            onclick="checkboxDestinationClick(<?php echo $row_destination_child['LocationId'] ?>,'<?php echo $row_destination_child['LocationName'] ?>',this)"
                            value="<?php echo $row_destination_child["LocationId"]; ?>"
                            ><?php echo $row_destination_child["LocationName"]; ?>
                          </label>
                        </div>
                        <?php
                      }?>
                </div>
              </div>
              <?php 
            }
            ?>
            </div>
          </div>
          
          
          
          <!-- end filtering --->
          <section class="promotions-tab">
            <div class="container">
              <div class="row">
                <div class="col-12 col-lg-3">
                  <div id="myDIV" class="pack-list-btns text-left">
                    <button type="button" class="btn btn-promo category-button active-2" data-filter = "category1"><i class="fa fa-th" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-promo category-button" data-filter = "category2"><i class="fa fa-list" aria-hidden="true"></i></button>
                  </div>
                </div>
                <div class="col-md-3">
                  <h5>Packages</h5>
                </div>
                <div class=" col-12 col-lg-6 float-right package-search d-none d-lg-block">
                  <!-- Search form -->
                  <form class="search">
                    <div class="search__wrapper">
                      <input type="text" name="" placeholder="Search for..." class="search__field">
                      <button type="submit" class="fa fa-search search__icon"></button>
                    </div>
                  </form>
                </div>
                <div class="col-md-12">
                  <div class="row mt-2">
                    
                    <div class="col-md-8" style="display: none;">
                      <p class="search-filter-p">
                        Search Filter :
                      </p>
                      <ul class="search-filter-ul">
                        <li> <p id="pDestinationSearchFilter"></p></li>
                        <li><p id="pTypeSearchFilter"></p></li>
                      </ul>
                      
                      
                    </div>
                    <div class="col-md-4">
                      
                    </div>
                  </div>
                </div>
                <div class="col-md-12 text-center">
                  <div id="items" class="container-fluid">
                    
                    <!-- BUTTON GROUP FOR CATEGORIES -->
                    <div  class="btn-group">
                      <div class="list-btns">
                      </div>
                    </div>
                    <!-- END BUTTON GROUP FOR CATEGORIES -->
                    
                    <div class="row">
                      <div class="col-md-12 category1 well">
                        <div class="row" id="package_row_list">
                          
                          
                          <div class="col-md-4"></div>
                          <div class="col-md-4"></div>
                          <div class="col-md-4"></div>
                        </div>
                        
                      </div>
                      
                      
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12 category2 well" id="package_col_list">
                        
                        
                      </div>
                      
                      
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- end filtering---->
          
        </div>
      </div>
      
    </div>
  </div>
</div>
</section>

<?php include('inc/footer.php')  ?>
<script>
var myApp = angular.module('myapp', ['rzModule']);
myApp.controller('TestController', TestController);
function TestController() {
var vm = this;
 vm.myChangeListener = function(sliderId) {

    bindPackageByDays(vm.priceSlider.value);
  };
  vm.priceSlider = {
    value: 0,
    options: {
      floor: 0,
      ceil: 20,
      onChange: vm.myChangeListener
    }
  }
}
</script>
<!--Package filtering-->
<!--<script src="js/jquery.isotope.min.js"></script>
<script>
$(function(){
var $container = $('#container'),
$checkboxes = $('#filters input');
$container.isotope({
itemSelector: '.item'
});
$checkboxes.change(function(){
var filters = [];
// get checked checkboxes values
$checkboxes.filter(':checked').each(function(){
filters.push( this.value );
});
// ['.red', '.blue'] -> '.red, .blue'
filters = filters.join(', ');
$container.isotope({ filter: filters });
});
});
</script>-->
<script>
$(document).ready(function () {
getAllPackage();
$('.check-box-pack-inn').hide();
$("input:checkbox").on("change", function () {
var a = $("input:checkbox:checked").map(function () {
return $(this).val()
}).get()
$("#tabela1 tr").hide();
var cities = $(".city").filter(function () {
var city = $(this).text(),
index = $.inArray(city, a);
return index >= 0
}).parent().show();
}).first().change()
});
</script>
<!-- list and grid view-->
<script type="text/javascript">
$(document).ready(function(){
// clicking button with class "category-button"
$(".well").not('.category1').hide("slow");
$(".well").filter('.category1').show("slow");
$(".category-button").click(function(){
// get the data-filter value of the button
var filterValue = $(this).attr('data-filter');
// show all items
$(".well").not('.'+filterValue).hide("slow");
// and then, show only items with selected data-filter value
$(".well").filter('.'+filterValue).show("slow");
});
});
</script>
<script type="text/javascript">
var btnContainer = document.getElementById("myDIV");
// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");
// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
btns[i].addEventListener("click", function() {
var current = document.getElementsByClassName("active-2");
current[0].className = current[0].className.replace(" active-2", "");
this.className += " active-2";
});
}
</script>
<script>
window.onscroll = function() {myFunction()};
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;
function myFunction() {
if (window.pageYOffset > sticky) {
header.classList.add("sticky");
} else {
header.classList.remove("sticky");
}
}
function checkThis(param){
bindPackage();
var id = $(param).attr('id');
if ($(param).prop('checked')==true){
$('#'+id+"-div").show();
}else{
$('#'+id+"-div").hide();
}
}
</script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script>
new UISearch( document.getElementById( 'sb-search' ) );
</script>
<script type="text/javascript">
</script>
</body>
</html>