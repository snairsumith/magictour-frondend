<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
include("../db/db.php");
$sql="";

if(!isset($_GET['type_id']) && !isset($_GET['destination_id'])){
 $data = array(
 	'resultCode' =>0 ,
 	'msg'=>'FillRecord'
 	 );
 echo json_encode($data);
}else if(isset($_GET['type_id']) && isset($_GET['destination_id'])){
	
	$typeId= $_GET['type_id'];
	$destinationId=$_GET["destination_id"];

	$sql = "SELECT DISTINCT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId INNER JOIN  package_tourtype_mapping on package_tourtype_mapping.PackageId=packages.PackageId  where packages.DestinationId=$destinationId and package_tourtype_mapping.TourTypeId IN($typeId)   order by packages.PackageId desc";

	$products_arr["records"]=array();
	$result_packages = $conn->query($sql);
	while($row_package = $result_packages->fetch_assoc()) {
		$package_deatils=array(
			'PackageId'=>$row_package['PackageId'],
			'PackageTitle'=>$row_package['PackageTitle'],
			'PackageDescription'=>$row_package['PackageDescription'],
			'Days'=>$row_package['Days'],
			'PackageImage'=>$row_package['PackageImage'],
			'PackageAmount'=>$row_package['PackageAmount'],
			'DestinationName'=>$row_package['DestinationName'],
			'PackageLocation'=>$row_package['PackageTitle']
		);
		array_push($products_arr["records"] , $package_deatils);
	}

	$data = array(
		'resultCode' => 1,
		'packages'=>$products_arr["records"],
		'msg'=>'Data Retrived'
	);
	echo json_encode($data);

}else if(isset($_GET['type_id'])){
	
	$typeId= $_GET['type_id'];

	$sql = "SELECT DISTINCT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId INNER JOIN package_tourtype_mapping on package_tourtype_mapping.PackageId=packages.PackageId  where  package_tourtype_mapping.TourTypeId IN($typeId)   order by packages.PackageId desc";

	$products_arr["records"]=array();
	$result_packages = $conn->query($sql);
	while($row_package = $result_packages->fetch_assoc()) {
		$package_deatils=array(
			'PackageId'=>$row_package['PackageId'],
			'PackageTitle'=>$row_package['PackageTitle'],
			'PackageDescription'=>$row_package['PackageDescription'],
			'Days'=>$row_package['Days'],
			'PackageImage'=>$row_package['PackageImage'],
			'PackageAmount'=>$row_package['PackageAmount'],
			'DestinationName'=>$row_package['DestinationName'],
			'PackageLocation'=>$row_package['PackageTitle']
		);
		array_push($products_arr["records"] , $package_deatils);
	}

	$data = array(
		'resultCode' => 1,
		'packages'=>$products_arr["records"],
		'msg'=>'Data Retrived'
	);
	echo json_encode($data);
}else if (isset($_GET['destination_id'])) {
	
	
	$destinationId=$_GET["destination_id"];

	$sql = "SELECT DISTINCT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId INNER JOIN package_tourtype_mapping on package_tourtype_mapping.PackageId=packages.PackageId  where packages.DestinationId=$destinationId   order by packages.PackageId desc";
	$products_arr["records"]=array();
	$result_packages = $conn->query($sql);
	while($row_package = $result_packages->fetch_assoc()) {
		$package_deatils=array(
			'PackageId'=>$row_package['PackageId'],
			'PackageTitle'=>$row_package['PackageTitle'],
			'PackageDescription'=>$row_package['PackageDescription'],
			'Days'=>$row_package['Days'],
			'PackageImage'=>$row_package['PackageImage'],
			'PackageAmount'=>$row_package['PackageAmount'],
			'DestinationName'=>$row_package['DestinationName'],
			'PackageLocation'=>$row_package['PackageLocation']
		);
		array_push($products_arr["records"] , $package_deatils);
	}

	$data = array(
		'resultCode' => 1,
		'packages'=>$products_arr["records"],
		'msg'=>'Data Retrived'
	);
	echo json_encode($data);
}else if(isset($_GET['packageDays'])){

	$noOfDays = $_GET['packageDays'];
	$sql = "SELECT DISTINCT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId   where packages.NoOfDays between 0 and $noOfDays  order by packages.PackageId desc";

	$products_arr["records"]=array();
	$result_packages = $conn->query($sql);
	while($row_package = $result_packages->fetch_assoc()) {
		$package_deatils=array(
			'PackageId'=>$row_package['PackageId'],
			'PackageTitle'=>$row_package['PackageTitle'],
			'PackageDescription'=>$row_package['PackageDescription'],
			'Days'=>$row_package['Days'],
			'PackageImage'=>$row_package['PackageImage'],
			'PackageAmount'=>$row_package['PackageAmount'],
			'DestinationName'=>$row_package['DestinationName'],
			'PackageLocation'=>$row_package['PackageTitle']
		);
		array_push($products_arr["records"] , $package_deatils);
	}

	$data = array(
		'resultCode' => 1,
		'packages'=>$products_arr["records"],
		'msg'=>'Data Retrived'
	);
	echo json_encode($data);
}

?>