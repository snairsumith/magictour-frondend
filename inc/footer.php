     <footer>
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-12 text-center mt-2 mb-4"> <img src="img/logo footer.png" class="img-fluid" alt="logo_footer"> </div>
            <div class="col-12 col-sm-3 col-lg-3">
              <div class="footer-contents">
                <h3>Contact Us</h3>
                <p>MAGIC TOURS <br>
                  PO Box 80915, Building 14, Shop 34<br>
                  Barwa Village, Al Wakra Road, <br>
                Doha, State of Qatar </p>
                <!--<div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="gloat-left"><span class="icon-callus"></span>+974 449 17 303 </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="float-left"><span class="icon-fax"></span>+974 449 17 308
                  </div>
                </div>
              </div>-->
              <ul class="cont-info">
                <li>+974 449 17 303</li>
                <li>+974 449 17 308</li>
              </ul>
              <p class="email">info@magictoursonline.com</p>
              <p class="web">www.magictoursonline.com</p>
              
            </div>
          </div>
          <div class="col-12 col-sm-2 ftr-brdr-cls">
            <div class="footer-contents">
              <h3>Booking</h3>
              <ul class="footer-list">
                <li>Best Deals</li>
                <li>BuyersProtection</li>
                <li>Roomers top 100</li>
              </ul>
            </div>
          </div>
          <div class="col-12 col-sm-3 ftr-brdr-cls">
            <div class="footer-contents">
              <h3>Support</h3>
              <ul class="footer-list">
                <li>Trust + Safety</li>
                <li>FAQs</li>
                <li>Testimonials</li>
                <li>Contact Us</li>
                <li>Hotel Cancellation Policy</li>
                <li>Cards</li>
                <li>my permissions</li>
              </ul>
            </div>
          </div>
          <div class="col-12 col-sm-2 ftr-brdr-cls">
            <div class="footer-contents">
              <h3>Company</h3>
              <ul class="footer-list">
                <li>About Us</li>
                <li>Press</li>
                <li>Blog</li>
                <li>Privacy Policy</li>
                <li>Terms & Conditions</li>
                <li>Partner With Us</li>
                <li>my permissions</li>
              </ul>
            </div>
          </div>
          <div class="col-12 col-sm-2 ftr-brdr-cls">
            <div class="footer-contents">
              <h3>Top Destinations</h3>
              <div class="row">
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/02special.jpg" class="img-fluid"></a>
                </div>
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/03special.jpg" class="img-fluid"></a>
                </div>
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/destinations.jpg" class="img-fluid"></a>
                </div>
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/03special.jpg" class="img-fluid"></a>
                </div>
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/destinations.jpg" class="img-fluid"></a>
                </div>
                <div class="col-4 col-md-4 ftr-company-pic">
                  <a href="#"><img src="img/02special.jpg" class="img-fluid"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="ftr-socio-icons">
            <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
          </div>
          <div class="clearfix"></div>
          <div class="ftr-copyright text-center">
            <p>© 2018 Magic Tours allright reserved</p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- JavaScript Libraries -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!--range slider-->
  <script src="js/angular.min.js"></script>
  <script src="js/rzslider.js"></script>
  <script src="js/main.js"></script>