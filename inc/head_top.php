<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Magic Tour</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="img/favicon.png" rel="icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/style2.css">
    <link rel="stylesheet" href="fonts/style.css">
    <!-- Main Stylesheet File -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/swipper.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="css/megamenu.css">
    <link rel="stylesheet" type="text/css" href="css/rzslider.css">
    <link rel="stylesheet" type="text/css" href="css/custom-arjun.css">
    <link rel="stylesheet" type="text/css" href="css/component.css">
    <link rel="stylesheet" type="text/css" href="css/new-custom.css">
    <link rel="stylesheet" type="text/css" href="css/tour.css">
  </head>
  <body id="body">
    <div class="wrapper">
      <section id="topbar" class="d-none d-lg-block">
        <div class="container clearfix">
          <div class="social-links float-left"> <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a> <a href="#" class="youtube"><i class="fa fa-youtube-play"></i></a> </div>
          <div class="contact-info float-right"> <a href="#" class="whyus">why us</a> <a href="#" class="feedback">Feedback</a> <a href="#" class="terms-conditions">Terms & Conditions </a>
          
          <span class="border-location">
            <select class="contact-selection-country">
              <option value="saab">INDIA</option>
              <option value="mercedes">Qatar</option>
              <option value="audi">option3</option>
            </select>
          </span> </div>
        </div>
      </section>
      <section id="topcontact" class="d-none d-lg-block">
        <div class="container clearfix">
          <div id="logo" class="float-left"> <a href="index.php" class="scrollto"><img src="img/logo.jpg" class="img-fluid"></a> </div>
          <div class="contact-list float-right">
            <div class="box">
              <div class="icon"> <i class="fa fa-phone"></i> </div>
              <p><span>CALL US</span> <br>
            +91 484 2783771 </p>
          </div>
          <div class="box">
            <div class="icon"> <i class="fa fa-envelope"></i> </div>
            <p><span>MAIL US</span><br>
          info@magictoursonline.com </p>
        </div>
        <div class="box no-rp">
          <div class="icon"> <i class="fa fa-map-marker"></i> </div>
          <p><span>LOCATE US</span><br>
        Kakkand, Kerala </p>
      </div>
     
    </div>
  </div>
</section>