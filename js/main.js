var baseurl="http://magictour.minusbugs.com/";
var imageBaseUrl="http://magicadmin.minusbugs.com/assets/PackageImages/";

var arryType=[];
var arryTypeName=[];

$(document).ready(function(){

});

function getAllPackage(){

    var html="";
    var html1="";
    url="http://magicadmin.minusbugs.com/ApiController/get_all_package";
    $.get( url, function( data ) {
      // if(data.allPackageDetails.length>0){
        var obj=jQuery.parseJSON(data);
      $.each(obj.allPackageDetails,function(index, el) {
        
        var imageUrl=el.PackageImage;

          html=html+'<div class="col-md-4"><div class="item-container item"> <img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers">';
          html=html+'<div class="prferedhotels"><div class="pull-right"><a href="#" class="btn btn-tp" role="button">';
          html=html+'<span class="package-amount"><i class="fa fa-inr"></i>'+el.PackageAmount+'</span></a></div><div class="prferedhotelstext">'+el.Days+'<br>';
          html=html+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html=html+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div><div class="overlay-offer overlay-tp">';
          html=html+'<div class="overlay-text"><p class="light-text">'+el.DestinationName+'</p><p>'+el.PackageLocation+'</p>';
          html=html+'<div class="d-block offer-icons"> <span class="icon-hotel"></span> <span class="icon-fod"></span> <span class="icon-adventur"></span>';
          html=html+'<span class="icon-flight"></span> <span class="icon-tour"></span> </div><div class="buttons"> <a href="packages-inner.php?id='+el.PackageId+'" class="btn btn-transparent">';
          html=html+'<i class="fa fa-link"></i> more</a>  </div>';
          html=html+'</div></div></div></div>';


          html1=html1+'<div class="row"><div class="col-md-4"><div class="item-container item"> ';
          html1=html1+'<img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers"></div>';
          html1=html1+'</div><div class="col-md-8"><div class="pack-gri-lis-l-c text-left">';
          html1=html1+'<h3>'+el.PackageTitle+'</h3><p>'+el.Days+'</p><p>'+el.PackageAmount+'</p><p>'+el.PackageLocation+'</p><p>Cities</p>';
          html1=html1+'<div class="pull-right"> <a href="packages-inner.php?id='+el.PackageId+'" class="btn btn-shade">';
          html1=html1+'<i class="fa fa-link"></i> more</a></div><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div></div>';
      });
    // }else{
    //   html="<h1>No Record Found</h1>";
    // }
     
      $("#package_row_list").html(html);
      $("#package_col_list").html(html1);
        
           
    });
}
function bindPackage(){
  var url="";
  var type_id=JSON.parse(($("#hdType").val()));
  var destination_id=parseInt($("#hdDestination").val());

  if(type_id.length==0 &&destination_id==0){

  }else if(type_id.length>0&&destination_id>0){
    url=baseurl+"/api/getallPackages.php?type_id="+type_id+"&destination_id="+destination_id;
  }else if(type_id.length>0&&destination_id==0){
    url=baseurl+"/api/getallPackages.php?type_id="+type_id;
  }else if(destination_id>0&&type_id.length==0){
    url=baseurl+"/api/getallPackages.php?destination_id="+destination_id;
  }

    var html="";
    var html1="";
    $.get( url, function( data ) {
       //console.log(data);
      if(data.packages.length>0){
      $.each(data.packages,function(index, el) {
       
        var imageUrl=el.PackageImage;

          html=html+'<div class="col-md-4"><div class="item-container item"> <img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers">';
          html=html+'<div class="prferedhotels"><div class="pull-right"><a href="#" class="btn btn-tp" role="button">';
          html=html+'<span class="package-amount"><i class="fa fa-inr"></i>'+el.PackageAmount+'</span></a></div><div class="prferedhotelstext">'+el.Days+'<br>';
          html=html+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html=html+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div><div class="overlay-offer overlay-tp">';
          html=html+'<div class="overlay-text"><p class="light-text">'+el.DestinationName+'</p><p>'+el.PackageLocation+'</p>';
          html=html+'<div class="d-block offer-icons"> <span class="icon-hotel"></span> <span class="icon-fod"></span> <span class="icon-adventur"></span>';
          html=html+'<span class="icon-flight"></span> <span class="icon-tour"></span> </div><div class="buttons"> <a href="packages-inner.php?id='+el.PackageId+'" class="btn btn-transparent">';
          html=html+'<i class="fa fa-link"></i> more</a>  </div>';
          html=html+'</div></div></div></div>';


          html1=html1+'<div class="row"><div class="col-md-4"><div class="item-container item"> ';
          html1=html1+'<img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers"></div>';
          html1=html1+'</div><div class="col-md-8"><div class="pack-gri-lis-l-c text-left">';
          html1=html1+'<h3>'+el.PackageTitle+'</h3><p>'+el.Days+'</p><p>'+el.PackageAmount+'</p><p>'+el.PackageLocation+'</p><p>Cities</p>';
          html1=html1+'<div class="pull-right"> <a href="#" class="btn btn-shade">';
          html1=html1+'<i class="fa fa-link"></i> more</a></div><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div></div>';
      });
    }else{
      html="<h1>No Record Found</h1>";
    }
     
      $("#package_row_list").html(html);
      $("#package_col_list").html(html1);
        
           
    });
    
    

  }


  function bindPackageByDays(days){
  var url=baseurl+"/api/getallPackages.php?packageDays="+days;
    var html="";
    var html1="";
    $.get( url, function( data ) {
      if(data.packages.length>0){
      $.each(data.packages,function(index, el) {
        console.log(el.DestinationName);
        var imageUrl=el.PackageImage;

          html=html+'<div class="col-md-4"><div class="item-container item"> <img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers">';
          html=html+'<div class="prferedhotels"><div class="pull-right"><a href="#" class="btn btn-tp" role="button">';
          html=html+'<span class="package-amount"><i class="fa fa-inr"></i>'+el.PackageAmount+'</span></a></div><div class="prferedhotelstext">'+el.Days+'<br>';
          html=html+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html=html+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div><div class="overlay-offer overlay-tp">';
          html=html+'<div class="overlay-text"><p class="light-text">'+el.DestinationName+'</p><p>'+el.PackageLocation+'</p>';
          html=html+'<div class="d-block offer-icons"> <span class="icon-hotel"></span> <span class="icon-fod"></span> <span class="icon-adventur"></span>';
          html=html+'<span class="icon-flight"></span> <span class="icon-tour"></span> </div><div class="buttons"> <a href="packages-inner.php?id='+el.PackageId+'" class="btn btn-transparent">';
          html=html+'<i class="fa fa-link"></i> more</a>  </div>';
          html=html+'</div></div></div></div>';


          html1=html1+'<div class="row"><div class="col-md-4"><div class="item-container item"> ';
          html1=html1+'<img src="'+imageBaseUrl+imageUrl+'" class="img-fluid image package-small-image" alt="special-offers"></div>';
          html1=html1+'</div><div class="col-md-8"><div class="pack-gri-lis-l-c text-left">';
          html1=html1+'<h3>'+el.PackageTitle+'</h3><p>'+el.Days+'</p><p>'+el.PackageAmount+'</p><p>'+el.PackageLocation+'</p><p>Cities</p>';
          html1=html1+'<div class="pull-right"> <a href="#" class="btn btn-shade">';
          html1=html1+'<i class="fa fa-link"></i> more</a></div><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>';
          html1=html1+'<span class="fa fa-star"></span><span class="fa fa-star"></span></div></div></div>';
      });
    }else{
      html="<h1>No Record Found</h1>";
    }
     
      $("#package_row_list").html(html);
      $("#package_col_list").html(html1);
        
           
    });
    
    

  }


function checkboxTourClick(id,name){
    // bindPackage();
    var isChecked = $('#chkBox_' + id).is(":checked");
    
    if(isChecked){
      arryType.push(id);
      arryTypeName.push(name);
      $("#hdType").val(JSON.stringify(arryType));
      
      bindPackage();
    }else{
      var index     = arryType.indexOf(id);
      var indexName = arryTypeName.indexOf(name);
      if (index > -1) {
          arryType.splice(index, 1);
          arryTypeName.splice(indexName,1);
      }
      $("#hdType").val(JSON.stringify(arryType));
      bindPackage();

    }
    $("#pTypeSearchFilter").text(JSON.stringify(arryTypeName));

    

     console.log("typeId",arryType);
  }

  function checkboxDestinationClick(id,name){

    $("#hdDestination").val(id);
    $("#pDestinationSearchFilter").text(name);
    bindPackage();
  }