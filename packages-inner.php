<?php
$id=$_GET["id"];
$imageUrl="http://magicadmin.minusbugs.com/assets/PackageImages/";
include('inc/head_top.php');
include('inc/head_menu.php');
//sql Main Packages
$sql_package = "SELECT packages.*,destinations.* FROM packages INNER JOIN destinations on packages.DestinationId=destinations.DestinationId  where PackageId=$id  order by packages.PackageId desc";
$sql = "select * from package_images where PackageId=$id";
$sql_tour="select tour_types.TypeName from tour_types inner join package_tourtype_mapping on tour_types.TypeId=package_tourtype_mapping.TourTypeId where package_tourtype_mapping.PackageId=$id";
$sql_package_inner="select * from package_inner where PackageId=$id";
$sql_package_hotel="select * from package_hotel where PackageId=$id";
$result_package_hotel = $conn->query($sql_package_hotel);
//Package Highlite
$sql_package_highlights="select * from package_highlights where PackageId=$id";
$result_package_highlights = $conn->query($sql_package_highlights);
//Package Highlite City 
$sql_package_highlights_city ="Select DISTINCT package_highlights.CityId,location.LocationName from package_highlights INNER JOIN location on location.LocationId=package_highlights.CityId WHERE package_highlights.PackageId=$id";
$result_package_highlights_city= $conn->query($sql_package_highlights_city);

$sql_package_faq="select * from package_faq where PackageId=$id";
$result_package_faq = $conn->query($sql_package_faq);
$sql_package_flight="select * from package_flight where PackageId=$id";
$result_package_flight = $conn->query($sql_package_flight);
$sql_package_inclusion_exclusion="select * from package_inclusion_exclusion where PackageId=$id";
$result_package_inclusion_exclusion= $conn->query($sql_package_inclusion_exclusion);
$sql_package_itinerary="select * from package_itinerary where PackageId=$id";
$result_package_itinerary= $conn->query($sql_package_itinerary);
$sql_package_train="select * from package_train where PackageId=$id";
$result_package_train= $conn->query($sql_package_train);
$sql_package_visa="select * from package_visa where PackageId=$id";
$result_package_visa= $conn->query($sql_package_visa);

//Package Price category_def
$sql_price_category_default ="select package_price.PackagePriceId,package_price.PackagePriceCategoryId,package_price_category.CategoryName from package_price INNER JOIN package_price_category on package_price_category.PackagePriceCategoryId=package_price.PackagePriceCategoryId where PriceType=1 and PackageId=$id order by package_price.PackagePriceCategoryId asc";
$result_price_category_default=$conn->query($sql_price_category_default);
$data_price_category = array();



$sql_price_category_suppliment ="select package_price.PackagePriceId,package_price.PackagePriceCategoryId,package_price_category.CategoryName from package_price INNER JOIN package_price_category on package_price_category.PackagePriceCategoryId=package_price.PackagePriceCategoryId where PriceType=2 and PackageId=$id order by package_price.PackagePriceCategoryId asc";
$result_price_category_suppliment=$conn->query($sql_price_category_suppliment);
//Package Price Common Details 
$sql_package_price="select * from package_price where PackageId=$id";
$result_price =$conn->query($sql_package_price);
if($row_price_basic=$result_price->fetch_assoc()){
  $basicPrice=$row_price_basic["BasicPrice"];
}
//Package Price Slab_DEFAULT
$sql_price_slab="select package_price.PackagePriceId,package_price.PackagePricePaxId,package_price_pax_slab.PaxSlabName from package_price INNER JOIN package_price_pax_slab on package_price_pax_slab.PackagePricePaxId=package_price.PackagePricePaxId where PriceType=1 and PackageId=$id order by package_price.PackagePricePaxId asc";
$result_price_slab_default = $conn->query($sql_price_slab);
//Package Price Slab_supplient
$sql_price_slab_suppliment="select package_price.PackagePriceId,package_price.PackagePricePaxId,package_price_pax_slab.PaxSlabName from package_price INNER JOIN package_price_pax_slab on package_price_pax_slab.PackagePricePaxId=package_price.PackagePricePaxId where PriceType=2 and PackageId=$id order by package_price.PackagePricePaxId asc";
$result_price_slab_suppliment = $conn->query($sql_price_slab_suppliment);


$sql_package_black_outdates="select * from package_black_outdates where PackageId=$id";
$result_black_outdates = $conn->query($sql_package_black_outdates);
$sql_package_links="select * from package_links where PackageId=$id";
$result_links = $conn->query($sql_package_links);
//Sql Package Locations
$sql_package_location="SELECT location.LocationName,location.LocationId,location.TypeOfLocation from package_location INNER JOIN location on location.LocationId=package_location.LocationId where package_location.PackageId=$id";
$result_locations = $conn->query($sql_package_location);
$packageCountry="";
$packageLocation="";

while($row_locations = $result_locations->fetch_assoc()) {
  if($row_locations['TypeOfLocation']==2){
     $packageCountry=$row_locations['LocationName'].','.$packageCountry;
  }else if($row_locations['TypeOfLocation']==3){
     $packageLocation=$row_locations['LocationName'].','.$packageLocation;
}
}

//Packagae Details
$result_package = $conn->query($sql_package);
if($row_pack = $result_package->fetch_assoc()) {
$packgaeMainImage=$imageUrl.$row_pack["PackageImage"];
$packageTitile=$row_pack["PackageTitle"];
$packageDays=$row_pack["Days"];
$packageCode=$row_pack["PackageCode"];
$packageDescription=$row_pack["PackageDescription"];
$packageAmount=$row_pack["PackageAmount"];
$packageStartDate=$row_pack["PackageStartDate"];
$packageEndDate=$row_pack["PackageEndDate"];
$packageDestination=$row_pack["DestinationName"];

}
//Inner Package Deatils
$result_package_inner = $conn->query($sql_package_inner);
if($row_package_inner = $result_package_inner->fetch_assoc()) {
$packageHighLite=$row_package_inner["Highlights"];
$packageHotel=$row_package_inner["Hotel"];
$packageFlight=$row_package_inner["Flight"];
$packageTrain=$row_package_inner["Train"];
$packageItinerary=$row_package_inner["Itinerary"];
$packagePrice=$row_package_inner["Price"];
$packageInclusion=$row_package_inner["Inclusion"];
$packagePolicy=$row_package_inner["Policy"];
$packageCancelPolicy=$row_package_inner["CancellationPolicy"];
$packageFaq=$row_package_inner["Faq"];
$packageVisa=$row_package_inner["Visa"];
}
//Tour Types
$result_tour_type = $conn->query($sql_tour);
$types="";
while($row_tour = $result_tour_type->fetch_assoc()) {
$types=$types.$row_tour["TypeName"].',';
}
?>
<div class="clearfix"></div>
<section class="about-header">
  <div class="container">
    <div class="row">
      <div class="offset-md-3 col-md-6 offset-right-md-3 text-center">
        <div class="about-banner-title text-center">
          <p>
            <a href="index.php">Home</a> > <a href="#">Package Details</a>
          </p>
          <h1>Packags</h1>
          
        </div>
      </div>
    </div>
  </div>
</section>
<section id="packages" class="packages-main">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-9" id="main">
        <div class="container">
          <div class="row">
            <div class="package-content">
              <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                  <?php
                  $result_img = $conn->query($sql);
                  if($result_img->num_rows>0){
                  $count=$result_img->num_rows;
                  ?>
                  <li data-target="#demo" data-slide-to="0" class="active"></li>
                  <?php
                  for($i=1;$i<=$count;$i++){
                  ?>
                  <li data-target="#demo" data-slide-to="<?php echo $i; ?>"></li>
                  <?php
                  }
                  }
                  ?>
                </ul>
                
                <!-- The slideshow -->
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="<?php echo $packgaeMainImage;  ?>" class="img-fluid package-large-img">
                  </div>
                  <?php
                  $result_img = $conn->query($sql);
                  while($row = $result_img->fetch_assoc()) {
                  $inner_imgs=$imageUrl.$row["ImageName"];
                  ?>
                  <div class="carousel-item">
                    <img src="<?php echo $inner_imgs;  ?>" class="img-fluid package-large-img">
                  </div>
                  <?php
                  }
                  ?>
                </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <section class="packages-in-sect-1">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <h6><?php echo $packageTitile;  ?></h6>
                <p><?php echo $packageDays;  ?></p>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <div class="col-md-6 text-right">
                <p><span class="color-red">Tour Code:</span> <?php echo $packageCode;  ?></p>
                <p><span class="color-red">Type: </span></p>
                <p> <?php echo $types; ?></p>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <p class="mag-flag"><?php echo $packageCountry;  ?></p>
                <p class="mag-location"><?php echo $packageLocation;  ?></p>
              </div>
              <div class="col-md-6">
                <p class="mag-calender"><?php echo $packageStartDate;  ?> - <?php echo $packageEndDate;  ?></p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"></div>
              <p class="mag-plane">2018 : Jan 26, Jan 30,Feb 25, Mar 22, 2019 : Jan 20, Mar 23,Feb 25 ...</p>
            </div>
          </div>
        </section>
        <div class="pcks-inn-types text-center">
          <h3>TYPES</h3>
          <div class="padding-sidebar">
            <ul>
              <?php
              $result_tour_type = $conn->query($sql_tour);
              $types="";
              while($row_tour = $result_tour_type->fetch_assoc()) {
              ?>
              <li> <input type="checkbox" value=""><?php echo $row_tour["TypeName"]; ?></li>
              <?php
              }
              ?>
              
            </ul>
          </div>
        </div>
        <section class="pack-inner-sect-2" id="pack-inn-tab">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="text-center" id="tabContentId">
                  <div id="myDIV" class="btn-group pack-main-btn">
                    <button type="button" id="Overview-class" class="btn btn-pack category-button active-3" data-filter = "all"> Overview </button>
                    <button type="button" id="Highlights-class" class="btn btn-pack category-button"  data-filter = "category2"> Highlights </button>
                    <button type="button" id="Hotels-class" class="btn btn-pack category-button" data-filter = "category3"> Hotels  </button>
                    <button type="button" id="Flight-class" class="btn btn-pack category-button" data-filter = "category4"> Flight  </button>
                    <button type="button" id="Train-class" class="btn btn-pack category-button" data-filter = "category5"> Train </button>
                    <button type="button" id="Itinerary-class" class="btn btn-pack category-button" data-filter = "category6"> Itinerary </button>
                    <button type="button" id="Map-class" class="btn btn-pack category-button" data-filter = "category7"> Map </button>
                    <button type="button" id="Price-class" class="btn btn-pack category-button" data-filter = "category8"> Price </button>
                    <button type="button" id="Inclusions-class" class="btn btn-pack category-button" data-filter = "category9"> Inclusions </button>
                    <button type="button" id="Policies-class" class="btn btn-pack category-button" data-filter = "category10"> Policies </button>
                    <button type="button" id="FAQ-class" class="btn btn-pack category-button" data-filter = "category12"> FAQ </button>
                    <button type="button" id="Visa-class" class="btn btn-pack category-button" data-filter = "category11"> Visa </button>
                  </div>
                </div>
                <div class="dummyClass">
                  <div class="row" id="Overview">
                    <div class="col-md-12 all category1 well">
                      <img src="img/icon/icon-1.png" class="img-fluid pcke-inn-left-img">
                      <h6>Journey Overview</h6>
                      <p><?php echo $packageDescription; ?>.</p>
                    </div>
                  </div>
                  <div class="row" id="Highlights">
                    <div class="col-md-12 all category2 well">
                      <h6>Journey Highlights</h6>
                      <img src="img/icon/icon-2.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <?php
                        while($row_highlit_city= $result_package_highlights_city->fetch_assoc()) {
                        ?>
                        <div class="col-md-6">
                          <p><?php echo $row_highlit_city["LocationName"]; ?></p>
                          <ul class="journey-lis" type="none">
                            <?php
                            $city_id=$row_highlit_city["CityId"];
                            $sql_package_highlights_title="Select * from package_highlights where CityId=$city_id and PackageId=$id";
                            $result_package_highlite_title = $conn->query($sql_package_highlights_title);
                            while($row_highlit_title= $result_package_highlite_title->fetch_assoc()) {
                            ?>
                            
                            <li><?php echo $row_highlit_title["HighlightsTitle"]; ?></li>
                            <?php
                            }
                            ?>
                          </ul>
                        </div>
                        <?php
                        }
                        ?>
                        
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Hotels">
                    <div class="col-md-12 all category3 well" style="overflow-x:auto;">
                      <h6>Hotels Used</h6>
                      <img src="img/icon/icon-3.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <?php
                          while($row_hotel = $result_package_hotel->fetch_assoc()) {
                          ?>
                          <table class="table-packa-ge">
                            
                            <tbody>
                              <tr>
                                <td><?php echo $row_hotel["Days"]; ?></td>
                                <td>:</td>
                                <td><?php echo $row_hotel["HotelAddress"]; ?></td>
                              </tr>
                              
                            </tbody>
                          </table>
                          <?php } ?>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Flight">
                    <div class="col-md-12 all category4 well" style="overflow-x:auto;">
                      <h6>Flights Details</h6>
                      <img src="img/icon/icon-4.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table-4">
                            <thead class="thead-dark-4">
                              <tr>
                                <th>Date</th>
                                <th>Flight No.</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Depature</th>
                                <th>Arrival</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              while($row_flight = $result_package_flight->fetch_assoc()) {
                              ?>
                              <tr>
                                <td><?php echo $row_flight["Date"]; ?></td>
                                <td><?php echo $row_flight["Flight"]; ?></td>
                                <td><?php echo $row_flight["FromLocation"]; ?></td>
                                <td><?php echo $row_flight["ToLocation"]; ?></td>
                                <td><?php echo $row_flight["Departure"]; ?></td>
                                <td><?php echo $row_flight["Arrival"]; ?></td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Train">
                    <div class="col-md-12 all category5 well" style="overflow-x:auto;">
                      <h6>Train Details</h6>
                      <img src="img/icon/icon-5.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table-4">
                            <thead class="thead-dark-4">
                              <tr>
                                <th>Date</th>
                                <th>Train</th>
                                <th>From</th>
                                <th>To</th>
                                <th></th>
                                <th>Depature</th>
                                <th>Arrival</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              while($row_train= $result_package_train->fetch_assoc()) {
                              ?>
                              <tr>
                                <td><?php echo $row_train["Date"]; ?></td>
                                <td><?php echo $row_train["Train"]; ?></td>
                                <td><?php echo $row_train["FromLocation"]; ?></td>
                                <td><?php echo $row_train["ToLocation"]; ?></td>
                                <td><?php echo $row_train["Departure"]; ?></td>
                                <td><?php echo $row_train["Arrival"]; ?></td>
                                <td><?php echo $row_train["Class"]; ?></td>
                                
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Itinerary">
                    <div class="col-md-12 all category6 well">
                      <h6>itinerary</h6>
                      <img src="img/icon/icon-6.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="ctgry-g-mrgn">
                            <?php
                            while($row_itinery= $result_package_itinerary->fetch_assoc()) {
                            ?>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="itinerary-inner">
                                  <div class="arrive-cls">
                                    <div  class="img-fluid  itinerary-day">
                                      <span class="itinerary-span-day">DAY</span>
                                      <p class="itinerary-span-day-no">
                                        <?php echo $row_itinery["Day"]; ?>
                                      </p>
                                    </div>
                                    <h5 class="pull-left"><?php echo $row_itinery["Title"]; ?></h5>
                                    <!-- <img src="img/icon/day1.png" class="img-fluid arrive-icon-img"> -->
                                  </div>
                                  <p class="pull-right mrgin-ri-clst"><?php echo "Travel in ".$row_itinery["ModeOfTravel"]; ?></p>
                                  <p class="pull-left mrgin-ri-clst"><?php echo $row_itinery["Description"]; ?></p>
                                  <p class="pull-left"><span class="color-black"><b><i class="fa fa-bed" aria-hidden="true"></i> Hotel Name : </b> <?php echo $row_itinery["Accommodations"]; ?></span></p>
                                  
                                  <p class="pull-right mrgin-ri-clst">
                                    <span class="color-black">
                                      <?php
                                      $food = $row_itinery["Food"];
                                      $str_food = explode(",", $food);
                                      $food_count = count($str_food);
                                      for ($i=0; $i < $food_count; $i++) {
                                      
                                      if($str_food[$i]=="Breakfast" ||$str_food[$i]=="Tea"){
                                      ?>
                                      <i class="fa fa-coffee" aria-hidden="true"></i>
                                      <b> <?php echo $str_food[$i]; ?> </b>&nbsp;
                                      <?php
                                      }else{ ?>
                                      <i class="fa fa-cutlery" aria-hidden="true"></i>
                                      <b> <?php echo $str_food[$i]; ?> </b>&nbsp;
                                      <?php
                                      }
                                      }
                                      ?>
                                    </span>
                                  </p>
                                  <div class="clearfix"></div>
                                  <p class="pull-left"><span class="color-black"><b>Optional Activities : </b></span></p>
                                  <p class="pull-left"><?php echo $row_itinery["OptionalActivities"]; ?> </p>
                                  
                                  
                                </div>
                              </div>
                            </div>
                            <?php }
                            ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Map">
                    <div class="col-md-12 all category7 well" style="overflow-x:auto;">
                      <h6>Map</h6>
                      <img src="img/icon/icon-7.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class=
                          "col-md-12">
                          <iframe src="https://www.google.com/maps/d/embed?mid=1o6Bk0ic_Xh5Q-_zVhwPsA6Qdrpg" width="100%" height="480"></iframe>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Price">
                    <div class="col-md-12 all category8 well" style="overflow-x:auto;">
                      <h6>Price</h6>
                      <img src="img/icon/icon-8.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <p class="pull-right">Basic Price</p>
                          <div class="clearfix"></div>
                          <p class="pull-right"><?php echo $basicPrice.' QAR';  ?>
                          <br>(Currency)</p>
                          <table class="table-5 text-center">
                            <thead class="thead-dark-5">
                              <tr>
                                <th rowspan="2">Category</th>
                                
                                <th colspan="2">01-05 Pax</th>
                                <th colspan="2">05-10 Pax</th>
                                <th colspan="2">10-15 Pax</th>
                                <th colspan="2">15-20 Pax</th>
                              </tr>
                              <tr>
                              
                                <th>Adults</th>
                                <th>Child</th>
                                <th>Adults</th>
                                <th>Child</th>
                                <th>Adults</th>
                                <th>Child</th>
                                <th>Adults</th>
                                <th>Child</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Deluxe</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                
                              </tr>
                              <tr>
                                <td>Luxary</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                <td>4</td>
                                <td>3</td>
                                
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Inclusions">
                    <div class="col-md-12 all category9 well">
                      <h6>Inclusion/ Exclusion</h6>
                      <img src="img/icon/icon-9.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <?php
                        $html_inc="";
                        $html_exc="";
                        while($row_inc_exc= $result_package_inclusion_exclusion->fetch_assoc()) {
                        if($row_inc_exc["TypeId"]==1){
                        $html_inc=$html_inc."<li>".$row_inc_exc["Options"]."</li>";
                        }else{
                        $html_exc=$html_exc."<li>". $row_inc_exc["Options"]."</li>";
                        }
                        }
                        ?>
                        <div class="col-md-6">
                          <p><span class="color-black"><b>Inclusions</b></span></p>
                          <ul class="journey-lis" type="none">
                            <?php echo $html_inc; ?>
                          </ul>
                        </div>
                        <div class="col-md-6">
                          <p><span class="color-black"><b>Exclusions</b></span></p>
                          <ul class="journey-lis" type="none">
                            <?php echo $html_exc; ?>
                          </ul>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                  <div class="row" id="Policies">
                    <div class="col-md-12 all category1 well">
                      <h6>Black Outdates</h6>
                      <img src="img/icon/icon-10.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          
                          <ul class="journey-lis" type="none">
                            <?php
                            
                            while($row_outdates= $result_black_outdates->fetch_assoc()) {
                            ?>
                            <li><?php echo $row_outdates["FromDate"] ?>-<?php echo $row_outdates["ToDate"] ?></li>
                            <?php
                            }
                            ?>
                            
                          </ul>
                        </div>
                        
                        
                      </div>
                    </div>
                    
                    
                  </div>
                  <div class="row" id="Policies">
                    <div class="col-md-12 all category10 well">
                      <h6>Booking Policy</h6>
                      <img src="img/icon/icon-10.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $packagePolicy; ?>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-md-12 all category13 well">
                      <h6>Cancelation Policy</h6>
                      <img src="img/icon/icon-13.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $packageCancelPolicy; ?>
                        </div>
                      </div>
                    </div>
                    
                    
                  </div>
                  <div class="row" id="FAQ">
                    <div class="col-md-12 all category12 well">
                      <h6>FAQ</h6>
                      <img src="img/icon/icon-14.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        
                        <?php
                        $i=1;
                        while($row_faq= $result_package_faq->fetch_assoc()) {
                        
                        ?>
                        <div class="col-md-12">
                          <h4><?php echo $i.") ".$row_faq["Question"];    ?></h4>
                          <p><?php echo $row_faq["Answer"];    ?></p>
                        </div>
                        <?php
                        $i++;
                        }
                        ?>
                        
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 all category15 well">
                      <h6>Links</h6>
                      <img src="img/icon/icon-15.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <?php
                          while($row_links= $result_links->fetch_assoc()) {
                          ?>
                          <p><?php echo $row_links["LinkName"] ?></p>
                          <?php
                          }?>
                        </div>
                      </div>
                    </div>
                    
                    
                  </div>
                  <div class="row" id="Visa">
                    <div class="col-md-12 all category11 well" style="overflow-x:auto;">
                      <h6>Visa Info</h6>
                      <img src="img/icon/icon-16.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        
                        <?php
                        
                        while($row_visa= $result_package_visa->fetch_assoc()) {
                        ?>
                        <div class="col-md-12">
                          <table class="table-visa-1">
                            
                            <tbody>
                              <tr>
                                <td><b>Country</b></td>
                                <td>:</td>
                                <td><?php echo $row_visa["Country"]; ?></td>
                                <td></td>
                                <td></td>
                                <td><b>Visa Head</b></td>
                                <td>:</td>
                                <td><?php echo $row_visa["VisaHead"]; ?></td>
                              </tr>
                              <tr>
                                <td><b>Nationality</b></td>
                                <td>:</td>
                                <td><?php echo $row_visa["Nationality"]; ?></td>
                                <td></td>
                                <td></td>
                                <td><b>Residency Country</b></td>
                                <td>:</td>
                                <td><?php echo $row_visa["Residency"]; ?></td>
                              </tr>
                              
                              
                              
                              
                            </tbody>
                          </table>
                          <p><span class="color-black"><b>Overview</b></span></p>
                          <p><?php echo $row_visa["Overview"]; ?></p>
                          <p><span class="color-black"><b>Requirments</b></span></p>
                          <p><?php echo $row_visa["Requirments"]; ?></p>
                          <p><span class="color-black"><b>Procedures</b></span></p>
                          <p><?php echo $row_visa["Procedures"]; ?></p>
                        </div>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12 all category14 well">
                      <h6>Important Contact</h6>
                      <img src="img/icon/icon-17.png" class="img-fluid pcke-inn-left-img">
                      <div class="row">
                        <div class="col-md-12">
                          <table class="table-contact-1">
                            <tbody>
                              <tr>
                                <td>Qatar Airways</td>
                                <td>:</td>
                                <td>78273237 </td>
                              </tr>
                              <tr>
                                <td>Qatar Embassy, Spain</td>
                                <td>:</td>
                                <td>97329732 </td>
                              </tr>
                              <tr>
                                <td>Police</td>
                                <td>:</td>
                                <td>999</td>
                              </tr>
                              <tr>
                                <td>Ambulance</td>
                                <td>:</td>
                                <td>9382 </td>
                              </tr>
                              <tr>
                                <td>Tourist Police</td>
                                <td>:</td>
                                <td>32323</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="d-none d-sm-block col-sm-3" id="sticky-sidebar">
        <div class="sticky-top">
          <div class="promotion-slider">
            <h3>EXCLUSIVE OFFERS</h3>
            <div id="promotionSlider" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#promotionSlider" data-slide-to="0" class="active"></li>
                <li data-target="#promotionSlider" data-slide-to="1"></li>
                <li data-target="#promotionSlider" data-slide-to="2"></li>
              </ul>
              
              <!-- The slideshow -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
                </div>
                <div class="carousel-item">
                  <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
                </div>
                <div class="carousel-item">
                  <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
                </div>
                <div class="carousel-item">
                  <img src="img/00offerbanner.jpg" class="img-fluid" alt="00offerbanner">
                </div>
                
              </div>
            </div>
            
          </div>
          
          <div class="sidebar-links mt-5">
            <h3>PACKAGE COST</h3>
            <div class="padding-sidebar">
              <ul>
                <li>Total Amount &#8377; <?php echo $packageAmount ?></li>
                
                
              </ul>
              <a href="#" class="btn btn-in-serv-side" role="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> ENQUIRY</a>
              
              <form class="packages-form">
                <p>Send This Tour To Your Email</p>
                <input type="text" class="form-control form-control-lg" placeholder="Email ID">
              </form>
              <a href="#" class="btn btn-package-side" role="button">SEND</a>
              <div class="pack-tour">
                <p>Share this tour via</p>
                <a href="#"><i class="fa fa-facebook-official fa-2x facebook-i" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram fa-2x instagram-i" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter-square fa-2x twitter-i" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-google-plus-square fa-2x google-i" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          </div> <!--end sticky-->
          
        </div>
      </div>
    </div>
  </section>
  <?php include('inc/footer.php') ?>
  <script>
  var myApp = angular.module('myapp', ['rzModule']);
  myApp.controller('TestController', TestController);
  function TestController() {
  var vm = this;
  vm.priceSlider = {
  value: 10,
  options: {
  floor: 0,
  ceil: 20
  }
  }
  }
  </script>
  <!--Package filtering-->
  <script src="js/jquery.isotope.min.js"></script>
  <script>
  $(function(){
  var $container = $('#container'),
  $checkboxes = $('#filters input');
  $container.isotope({
  itemSelector: '.item'
  });
  $checkboxes.change(function(){
  var filters = [];
  // get checked checkboxes values
  $checkboxes.filter(':checked').each(function(){
  filters.push( this.value );
  });
  // ['.red', '.blue'] -> '.red, .blue'
  filters = filters.join(', ');
  $container.isotope({ filter: filters });
  });
  });
  </script>
  <script type="text/javascript">
  $(document).ready(function(){
  // clicking button with class "category-button"
  $(".category-button").click(function(){
  // get the data-filter value of the button
  var filterValue = $(this).attr('data-filter');
  // show all items
  if(filterValue == "all")
  {
  $(".all").show("slow");
  }
  else
  {
  // hide all items
  $(".all").not('.'+filterValue).hide("slow");
  // and then, show only items with selected data-filter value
  $(".all").filter('.'+filterValue).show("slow");
  }
  });
  });
  </script>
  <script type="text/javascript">
  var btnContainer = document.getElementById("myDIV");
  // Get all buttons with class="btn" inside the container
  var btns = btnContainer.getElementsByClassName("btn");
  // Loop through the buttons and add the active class to the current/clicked button
  for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active-3");
  current[0].className = current[0].className.replace(" active-3", "");
  this.className += " active-3";
  });
  }
  </script>
  <script>
  window.onscroll = function() {myFunction();};
  var header = document.getElementById("myHeader");
  var sticky = header.offsetTop;
  function myFunction() {
  if (window.pageYOffset > sticky) {
  header.classList.add("sticky");
  } else {
  header.classList.remove("sticky");
  }
  }
  function sticktothetop() {
  var window_top = $(window).scrollTop();
  var top = $('.padding-sidebar').offset().top;
  if (window_top >= top) {
  $('#tabContentId').addClass('stick');
  } else {
  $('#tabContentId').removeClass('stick');
  }
  }
  $(function() {
  $(window).scroll(sticktothetop);
  sticktothetop();
  });
  </script>
  <style>
  .stick {
  margin-left:4%;
  margin-top: 0;
  position: fixed;
  top: 50px;
  z-index: 9999;
  transition:all 0.35s ease-in;
  -webkit-border-radius: 0 0 10px 10px;
  border-radius: 0 0 10px 10px;
  }
  </style>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/1.1.6/waypoints.min.js"></script>
  <script src="js/classie.js"></script>
  <script src="js/uisearch.js"></script>
  <script>
  $('#Overview').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Overview-class").addClass("active-3");
  }, { offset: 100 });
  $('#Highlights').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Highlights-class").addClass("active-3");
  }, { offset: 100 });
  $('#Hotels').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Hotels-class").addClass("active-3");
  }, { offset: 100 });
  $('#Flight').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Flight-class").addClass("active-3");
  }, { offset: 100 });
  $('#Train').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Train-class").addClass("active-3");
  }, { offset: 100 });
  $('#Itinerary').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Itinerary-class").addClass("active-3");
  }, { offset: 100 });
  $('#Map').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Map-class").addClass("active-3");
  }, { offset: 100 });
  $('#Price').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Price-class").addClass("active-3");
  }, { offset: 100 });
  $('#Inclusions').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Inclusions-class").addClass("active-3");
  }, { offset: 100 });
  $('#Policies').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Policies-class").addClass("active-3");
  }, { offset: 100 });
  $('#Visa').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#Visa-class").addClass("active-3");
  }, { offset: 100 });
  $('#FAQ').waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#FAQ-class").addClass("active-3");
  }, { offset: 100 });
  /*$.each( $('.dummyClass'), function(i, left) {
  $('div', left).each(function() {
  $('#'+id).waypoint(function() {
  $("#myDIV button").removeClass("active-3");
  $("#"+id+"-class").addClass("active-3");
  }, { offset: 100 });
  });
  })*/
  new UISearch( document.getElementById( 'sb-search' ) );
  </script>
</body>
</html>