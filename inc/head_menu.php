<?php 
 include("db/db.php");
 $sql_destination="select * from location where TypeOfLocation=1";
 $result_destination = $conn->query($sql_destination);
 $sql_tour_type="select * from tour_types";
 $result_tour_type = $conn->query($sql_tour_type);
?>
<!--==========================
Header
============================-->
<header class="header" id="myHeader">
  <div class="container">
    <nav class="navbar navbar-expand-lg">
      <a href="index.php" class="scrollto d-lg-none"><img src="img/logo.jpg" class="img-fluid"></a>
      <a class="navbar-brand d-none d-lg-block" href="index.php"><span class="h-icon icon-home"></span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span><i class="fa fa-bars"></i> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"> <a class="nav-link" href="about.html">About Us</a> </li>
          <li class="nav-item dropdown">
            <!--          <a class="nav-link dropdown-toggle" data-target="services.html"  href="services.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>-->
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
            <div class="dropdown-menu" >
              <div class="container">
                <div class="row bg-white">
                  <div class="col-12 col-sm-3 sub-menu-item">
                    <a class="nav-item main-list-mmenu"  href="#">Services</a>
                    <a class="nav-item"  href="#">Visa Documentations & Support</a>
                    <a class="nav-item" href="#">Airline Tickets</a>
                    <a class="nav-item" href="#">Holiday Packages</a>
                    <a class="nav-item" href="#">Fixed Departure Tours</a>
                    <a class="nav-item" href="#">Charter Flights & Air Ambulance</a> </div>
                    <div class="col-12 col-sm-3 sub-menu-item">
                      <a class="nav-item" href="#">Hotel Reservations</a>
                      <a class="nav-item" href="#">Meet & Greet Services</a>
                      <a class="nav-item" href="#">Travel Insurance</a>
                      <a class="nav-item" href="#">International Driving Licence</a>
                      <a class="nav-item" href="#">Cruises & Yachts</a> </div>
                      <div class="col-12 col-sm-3 sub-menu-item">
                        <a class="nav-item" href="#">Adventure & Sports Tours</a>
                        <a class="nav-item" href="#">Medical Tourism</a>
                        <a class="nav-item" href="#">Golf Packages</a>
                        <a class="nav-item" href="#">Train Tours</a>
                        <a class="nav-item" href="#">Car Hire & Transfers</a> </div>
                        <div class="col-12 col-sm-3 sub-menu-item">
                          <a class="nav-item" href="#">Ski Packages</a>
                          <a class="nav-item" href="#">Pilgrimage Tours</a>
                          <a class="nav-item" href="#">MICE</a>
                          <a class="nav-item" href="#">Educational Tours</a>
                          <a class="nav-item" href="#">Tourist Visas (Qatar & UAE)</a> </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Packages</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <div class="container-fluid">
                        <div class="row">
                          <ul class="nav nav-pills flex-column nav-stacked col-md-3" id="nav-tab" role="tablist">
                            <li><a class="nav-item nav-link active" id="nav-home-tab" data-toggle="pill" href="#tab_a" role="tab" aria-controls="nav-home" aria-selected="true">Regional Categories</a></li>
                            <li> <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="pill" href="#tab_b" role="tab" aria-controls="nav-profile" aria-selected="false">Tour Type</a></li>
                          </ul>
                          <div class="tab-content col-md-9">
                            <div class="tab-pane active" id="tab_a">
                              <div class="container">
                                <div class="row bg-white">
                                  <div class="col-12 col-sm-4 sub-menu-item">
                                    <a class="nav-item main-list-mmenu"  href="package-search.php">Packages</a>
                                  <?php
                                    $counter_destination=1;
                                    while($row_destination = $result_destination->fetch_assoc()) {
                                    if($counter_destination==5){ 
                                  ?>  

                                  </div>  
                                  <div class="col-12 col-sm-4 offset-md-right-4 sub-menu-item">  
                                    <a class="nav-item"  href="#"><?php echo $row_destination["LocationName"] ?></a>
                                  <?php
                                    }else{
                                  ?>
                                    <a class="nav-item"  href="#"><?php echo $row_destination["LocationName"] ?></a>
                                  <?php
                                    }
                                  
                                  $counter_destination=$counter_destination+1;
                                }
                                ?>
                                </div>
                              </div>
                            </div>
                          </div>
                            <div class="tab-pane" id="tab_b">
                              <div class="container">
                                <div class="row bg-white">
                                  <div class="col-12 col-sm-4 sub-menu-item">
                                    <?php
                                       while($row_tourtype = $result_tour_type->fetch_assoc()) {
                                    ?>
                                    <a class="nav-item"  href="#"><?php echo $row_tourtype["TypeName"]; ?></a>
                                    <?php
                                    }
                                    ?>

                                   
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- tab content -->
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="nav-item dropdown"> <!--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Cruises </a>-->
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cruises</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <div class="container-fluid">
                      <div class="row">
                        <ul class="nav nav-pills flex-column nav-stacked col-md-3" id="nav-tab" role="tablist">
                          <li><a class="nav-item nav-link active" id="nav-home-tab" data-toggle="pill" href="#tab_a" role="tab" aria-controls="nav-home" aria-selected="true">Regional Categories</a></li>
                          <li> <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="pill" href="#tab_b1" role="tab" aria-controls="nav-profile" aria-selected="false">Cruises</a></li>
                        </ul>
                        <div class="tab-content col-md-9">
                          <div class="tab-pane active" id="tab_a">
                            <div class="container">
                              <div class="row bg-white">
                                <div class="col-12 col-sm-4 sub-menu-item">
                                  <a class="nav-item main-list-mmenu"  href="#">Cruises</a>
                                  <a class="nav-item"  href="#">Africa</a>
                                  <a class="nav-item" href="#">Asia</a>
                                  <a class="nav-item" href="#">Europe</a>
                                  <a class="nav-item" href="#">South America</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab_b1">
                            <div class="container">
                              <div class="row bg-white">
                                <div class="col-12 col-sm-4 sub-menu-item"> <a class="nav-item"  href="#">Celestyal Cruises</a> </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- tab content -->
                      </div>
                    </div>
                  </div>
                </li>
                <li class="nav-item dropdown"> <!--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MICE</a>-->
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MICE</a>
                <div class="dropdown-menu" >
                  <div class="container">
                    <div class="row bg-white">
                      <div class="col-12 col-sm-3 sub-menu-item">
                        <a class="nav-item main-list-mmenu"  href="#">MICE</a>
                        <a class="nav-item"  href="#">General Overview</a>
                        <a class="nav-item" href="#">Why Magic Tours</a>
                        <a class="nav-item" href="#">Our Services</a>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="nav-item"> <a class="nav-link" href="#">Travel Insurance</a> </li>
              <li class="nav-item"> <a class="nav-link" href="#">Visa</a> </li>
              <li class="nav-item"> <a class="nav-link" href="contact.html">Contact Us</a> </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                <div class="dropdown-menu" >
                  <div class="container">
                    <div class="row bg-white">
                      <div class="col-12 col-sm-3 sub-menu-item">
                        <a class="nav-item"  href="#">Destinations</a>
                        <a class="nav-item" href="#">Train Tours</a>
                        <a class="nav-item" href="#">Ski Holidays</a>
                        <a class="nav-item" href="#">Health & Spa</a>
                        <a class="nav-item" href="#">Air Charter</a>
                        <a class="nav-item" href="#">Honey Moon Packages</a>
                        <a class="nav-item" href="#">Promotions</a>
                        <a class="nav-item" href="#">News Letters</a>
                        <a class="nav-item" href="#">Testimonials</a>
                        <a class="nav-item" href="#">Careers</a> </div>
                        <div class="col-12 col-sm-9">
                          <div class="" aria-labelledby="navbarDropdown">
                            <div class="">
                              <div class="row">
                                <ul class="nav nav-pills flex-column nav-stacked col-md-3" id="nav-tab" role="tablist">
                                  <li><a class="nav-item nav-link active" id="nav-home-tab" data-toggle="pill" href="#tab_a1" role="tab" aria-controls="nav-home" aria-selected="true">Explore Qatar</a></li>
                                  <li> <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="pill" href="#tab_b2" role="tab" aria-controls="nav-profile" aria-selected="false">Tools</a></li>
                                  <li> <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="pill" href="#tab_b3" role="tab" aria-controls="nav-profile" aria-selected="false">Link</a></li>
                                </ul>
                                <div class="tab-content col-md-9">
                                  <div class="tab-pane active" id="tab_a1">
                                    <div class="container">
                                      <div class="row bg-white">
                                        <div class="col-12 col-sm-4 sub-menu-item">
                                          <a class="nav-item"  href="#">General Facts</a>
                                          <a class="nav-item" href="#">Entry Formalities</a>
                                          <a class="nav-item" href="#">Qatar Hotels</a>
                                          <a class="nav-item" href="#">Hotel Reservations</a>
                                          <a class="nav-item"  href="#">Adventures & Safari</a>
                                          <a class="nav-item" href="#">Dhow Cruises</a>
                                          <a class="nav-item" href="#">Water Sports</a>
                                          <a class="nav-item" href="#">Golfing</a>
                                        </div>
                                        <div class="col-12 col-sm-4 sub-menu-item">
                                          <a class="nav-item" href="#">Game Fishing</a>
                                          <a class="nav-item" href="#">Tour Packages</a>
                                          <a class="nav-item" href="#">Photo Gallery</a>
                                          <a class="nav-item" href="#">Heritage Tours</a>
                                          <a class="nav-item" href="#">All</a>
                                          <a class="nav-item" href="#">Desert Safari</a>
                                          <a class="nav-item" href="#">Heritage Tours</a>
                                          <a class="nav-item" href="#">Heritage Tours</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="tab_b2">
                                    <div class="container">
                                      <div class="row bg-white">
                                        <div class="col-12 col-sm-6 sub-menu-item">
                                          <a class="nav-item"  href="#">World Time</a>
                                          <a class="nav-item"  href="#">World Climate</a>
                                          <a class="nav-item"  href="#">Visa Information</a>
                                          <a class="nav-item"  href="#">Foreign Exchange</a>
                                          <a class="nav-item"  href="#">Destination Informations</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="tab_b3">
                                    <div class="container">
                                      <div class="row bg-white">
                                        <div class="col-12 col-sm-6 sub-menu-item">
                                          <a class="nav-item"  href="#">Airline Sites</a>
                                          <a class="nav-item"  href="#">Cruise Liners</a>
                                          <a class="nav-item"  href="#">Embassy Sites</a>
                                          <a class="nav-item"  href="#">Tourism Department</a>
                                          <a class="nav-item"  href="#">Hotels</a>
                                          <a class="nav-item"  href="#">Visa Forms</a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- tab content -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
          <div id="sb-search" class="sb-search d-none d-lg-block">
            <form>
              <input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
              <input class="sb-search-submit" type="submit" value="">
              <span class="sb-icon-search"></span>
            </form>
          </div>
          
          
        </div>
      </header>